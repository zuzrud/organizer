package com.zuz.navigator;

import java.time.LocalDate;

import com.zuz.util.MonthMapper;
import com.zuz.views.MainWindow;
import javafx.geometry.Insets;
import javafx.scene.Parent;
import javafx.scene.control.Button;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;

public class Navigator {

    private MainWindow mainWindow;

    private Button buttonMonth;
    private Button buttonDay;
    private Button buttonYear;

    public Navigator(MainWindow mainWindow) {
        this.mainWindow = mainWindow;
    }

    public Parent getMainMenu() {
        GridPane mainMenu = new GridPane();
        mainMenu.setStyle("-fx-padding: 10;" +
            "-fx-border-style: solid inside;" +
            "-fx-border-width: 2;" +
            "-fx-border-insets: 5;" +
            "-fx-border-radius: 5;" +
            "-fx-border-color: gray;");
        mainMenu.setPadding(new Insets(10, 10, 10, 10));
        mainMenu.setVgap(8);
        mainMenu.setHgap(10);

        // Set column widths
        ColumnConstraints colD = new ColumnConstraints();
        colD.setPercentWidth(15);
        ColumnConstraints colM = new ColumnConstraints();
        colM.setPercentWidth(15);
        ColumnConstraints colY = new ColumnConstraints();
        colY.setPercentWidth(15);
        ColumnConstraints colEmpty = new ColumnConstraints();
        colEmpty.setPercentWidth(45);
        ColumnConstraints colNotes = new ColumnConstraints();
        colNotes.setPercentWidth(10);
        mainMenu.getColumnConstraints().addAll(colD, colM, colY, colEmpty, colNotes);

        buttonDay = new Button();
        buttonDay.setPrefWidth(100);
        buttonDay.setOnAction(e -> mainWindow.setCenter(mainWindow.getDayWindow().makeCenter()));
        buttonMonth = new Button();
        buttonMonth.setPrefWidth(100);
        buttonMonth.setOnAction(e -> mainWindow.setCenter(mainWindow.getMonthWindow().makeCenter()));
        buttonYear = new Button();
        buttonYear.setPrefWidth(100);
        buttonYear.setOnAction(e -> mainWindow.setCenter(mainWindow.getYearWindow().makeCenter()));

        Button buttonNotes = new Button("Notatki");
        buttonNotes.setOnAction(e -> mainWindow.setCenter((Pane) mainWindow.getNoteTable().createView()));

        GridPane.setConstraints(buttonDay, 0, 0);
        GridPane.setConstraints(buttonMonth, 1, 0);
        GridPane.setConstraints(buttonYear, 2, 0);
        GridPane.setConstraints(buttonNotes, 4, 0);

        mainMenu.getChildren().addAll(buttonDay, buttonMonth, buttonYear, buttonNotes);
        return mainMenu;
    }

    public void setDay(LocalDate day){
        buttonDay.setText(Integer.valueOf(day.getDayOfMonth()).toString());
        buttonMonth.setText(MonthMapper.months.get(day.getMonthValue()));
        buttonYear.setText(Integer.valueOf(day.getYear()).toString());
    }

}
