package com.zuz.viewutil;

import java.util.List;
import java.util.Optional;

import com.zuz.database.DataSource;
import com.zuz.util.MessageBox;
import javafx.beans.value.ChangeListener;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javafx.event.ActionEvent;
import javafx.geometry.Insets;
import javafx.scene.Parent;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;

public abstract class GenericTable<T> {

    private TableView<T> tableView;

    private ObservableList<T> masterData = FXCollections.observableArrayList();

    private Parent bottom;

    private Button deleteButton;

    private Button editButton;

    private Button addButton;

    private TextField searchText;

    public Parent createView() {

        // Create a BorderPane with a Text node in each of the five regions
        BorderPane root = new BorderPane();
        // Set the Size of the VBox
        root.setPrefSize(getViewPrefWidth(), getViewPrefHeight());
        root.setMaxSize(1200, 900);
        // Set the Style-properties of the BorderPane
        root.setStyle(getStyle());

        Parent top = getTop();
        root.setTop(top);

        tableView = getTableView();
        tableView.setItems(loadData());
        root.setCenter(tableView);

        bottom = getBottom();
        root.setBottom(bottom);

        Parent right = getRight();
        root.setRight(right);

        return root;
    }

    protected String getStyle() {
        return "-fx-padding: 10;" +
            "-fx-border-style: solid inside;" +
            "-fx-border-width: 2;" +
            "-fx-border-insets: 5;" +
            "-fx-border-radius: 5;" +
            "-fx-border-color: gray;";
    }

    protected int getViewPrefHeight() {
        return 500;
    }

    protected int getViewPrefWidth() {
        return 500;
    }

    protected Parent getRight() {
        return null;
    }

    private Parent getTop() {
        Label title = new Label(getTitle());
        title.setTextFill(Color.BLUE);
        title.setStyle("-fx-font-size: 14pt; -fx-font-weight: bold;");

        VBox vBox = new VBox();
        HBox titleBox = new HBox(title);
        titleBox.setSpacing(15);
        vBox.getChildren().add(titleBox);

        if (findVisible()) {
            Label search = new Label("Szukaj");
            searchText = new TextField();

            HBox searchBox = new HBox(search, searchText);
            searchBox.setSpacing(10);
            searchBox.setPadding(new Insets(10));

            vBox.getChildren().add(searchBox);
            vBox.setSpacing(10);
        }

        return vBox;
    }

    protected boolean findVisible() {
        return true;
    }

    protected abstract String getTitle();

    protected Parent getBottom() {
        addButton = new Button("Dodaj");
        addButton.setOnAction(event -> add(event));
        editButton = new Button("Zmień");
        editButton.setOnAction(event -> edit(event));
        deleteButton = new Button("Skasuj");
        deleteButton.setOnAction(event -> delete(event));

        HBox hBox = new HBox(addButton, editButton, deleteButton);
        hBox.setSpacing(10);
        hBox.setPadding(new Insets(10));
        return hBox;
    }

    private void edit(ActionEvent event) {
        Optional<T> selectedItem = getSelectedItem();
        selectedItem.ifPresent(t -> {
            Optional<T> item = getEnteredData(selectedItem.get());
            item.ifPresent(t1 -> {
                editItem(t1, selectedItem.get());
            });
        });
    }

    public Optional<T> getSelectedItem() {
        ObservableList<T> selectedItems = getSelectedItems();
        return Optional.ofNullable(selectedItems.stream().findFirst().orElse(null));
    }

    private void editItem(T t, T oldT) {
        try {
            t = (T) getDataSource().editItem(t, oldT);
            masterData.remove(oldT);
            masterData.add(t);
            selectItem(t);
        } catch (Exception e) {
            MessageBox.showError("Edycja danych", "Błąd zapisu do bazy danych", e.getMessage());
        }
    }

    private void add(ActionEvent actionEvent) {
        Optional<T> item = getEnteredData();
        item.ifPresent(t -> {
            addItem(t);
        });
    }

    private void addItem(T t) {
        try {
            t = (T) getDataSource().addItem(t);
            masterData.add(t);
            selectItem(t);
        } catch (Exception e) {
            MessageBox.showError("Dodawanie danych", "Błąd zapisu do bazy danych", e.getMessage());
        }
    }

    private void selectItem(T t) {
        int row = tableView.getItems().indexOf(t);
        tableView.requestFocus();
        tableView.getSelectionModel().select(row);
        tableView.getFocusModel().focus(row);
    }

    protected abstract Optional<T> getEnteredData();

    protected abstract Optional<T> getEnteredData(T item);

    public abstract TableView<T> getTableView();

    public abstract List<T> getDataForDisplay();

    private ObservableList<T> loadData() {
        masterData.clear();
        masterData.addAll(getDataForDisplay());

        // 1. Wrap the ObservableList in a FilteredList (initially display all data).
        FilteredList<T> filteredData = new FilteredList<>(masterData, p -> true);
        // 2. Set the filter Predicate whenever the filter changes.
        if (findVisible()) {
            searchText.textProperty().addListener(getStringChangeListener(filteredData));
        }
        // 3. Wrap the FilteredList in a SortedList.
        SortedList<T> sortedData = new SortedList<>(filteredData);

        // 4. Bind the SortedList comparator to the TableView comparator.
        // 	  Otherwise, sorting the TableView would have no effect.
        sortedData.comparatorProperty().bind(tableView.comparatorProperty());

        return sortedData;
    }

    public abstract ChangeListener<String> getStringChangeListener(FilteredList<T> filteredData);

    private void delete(ActionEvent actionEvent) {
        ObservableList<T> selectedItems = getSelectedItems();
        selectedItems.forEach(t -> {
            deleteItem(t);
        });
    }

    protected ObservableList<T> getSelectedItems() {
        ObservableList<T> selectedItems;
        selectedItems = tableView.getSelectionModel().getSelectedItems();
        return selectedItems;
    }

    private void deleteItem(T t) {
        if (MessageBox.confirmation("Kasowanie danych", "Czy jesteś pewien, że chcesz skasować te dane?")) {
            try {
                if (getDataSource().deleteItem(t)) {
                    masterData.remove(t);
                }
            } catch (Exception e) {
                MessageBox.showError("Usuwanie danych", "Błąd kasowania z bazy danych", e.getMessage());
            }
        } else {
            // ... user chose CANCEL or closed the dialog
        }

    }

    public ObservableList<T> getMasterData() {
        return masterData;
    }

    public void setMasterData(ObservableList<T> masterData) {
        this.masterData = masterData;
    }

    public abstract DataSource getDataSource();
}
