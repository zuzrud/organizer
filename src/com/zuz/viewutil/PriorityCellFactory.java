package com.zuz.viewutil;

import com.zuz.model.PriorityEnum;
import javafx.scene.control.ListView;
import javafx.util.Callback;

public class PriorityCellFactory implements Callback<ListView<PriorityEnum>, PriorityEnumCell> {

    @Override
    public PriorityEnumCell call(ListView<PriorityEnum> listview) {
        return new PriorityEnumCell();
    }
}
