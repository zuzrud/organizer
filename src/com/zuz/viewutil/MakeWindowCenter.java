package com.zuz.viewutil;

import javafx.scene.Parent;

public interface MakeWindowCenter {

    Parent makeCenter();

}
