package com.zuz.viewutil;

import com.zuz.model.ReminderTimeEnum;
import javafx.scene.control.ListCell;

public class ReminderTimeEnumCell extends ListCell<ReminderTimeEnum> {

    @Override
    protected void updateItem(ReminderTimeEnum item, boolean empty) {
        super.updateItem(item, empty);
        String name = null;
        // Format name
        if (item == null || empty) {
        } else {
            name = item.getName();
        }

        this.setText(name);
        //setGraphic(null);
    }
}
