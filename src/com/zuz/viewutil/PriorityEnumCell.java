package com.zuz.viewutil;

import com.zuz.model.PriorityEnum;
import javafx.scene.control.ListCell;

public class PriorityEnumCell extends ListCell<PriorityEnum> {

    @Override
    protected void updateItem(PriorityEnum item, boolean empty) {
        super.updateItem(item, empty);
        String name = null;
        // Format name
        if (item == null || empty) {
        } else {
            name = item.getName();
        }

        this.setText(name);
        //setGraphic(null);
    }
}
