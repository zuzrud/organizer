package com.zuz.viewutil;

import javafx.scene.control.ListCell;

public class DaysHoursCell extends ListCell<String> {

    @Override
    protected void updateItem(String item, boolean empty) {
        super.updateItem(item, empty);
        String name = null;
        // Format name
        if (item == null || empty) {
        } else {
            name = item;
        }

        this.setText(name);
        //setGraphic(null);
    }
}
