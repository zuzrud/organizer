package com.zuz.viewutil;

import com.zuz.model.SpendingCategoryEnum;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.util.Callback;

public class SpendingCategoryCellFactory implements Callback<ListView<SpendingCategoryEnum>, ListCell<SpendingCategoryEnum>> {

    @Override
    public ListCell<SpendingCategoryEnum> call(ListView<SpendingCategoryEnum> listview) {
        return new SpendingCategoryEnumCell();
    }
}
