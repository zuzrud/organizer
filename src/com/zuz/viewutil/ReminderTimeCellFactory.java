package com.zuz.viewutil;

import com.zuz.model.ReminderTimeEnum;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.util.Callback;

public class ReminderTimeCellFactory implements Callback<ListView<ReminderTimeEnum>, ListCell<ReminderTimeEnum>> {

    @Override
    public ListCell<ReminderTimeEnum> call(ListView<ReminderTimeEnum> listview) {
        return new ReminderTimeEnumCell();
    }
}
