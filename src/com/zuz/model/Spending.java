package com.zuz.model;

import java.time.LocalDate;

public class Spending implements Id{

    private Long id;
    private LocalDate date;
    private String spending;
    private Integer amount;
    private SpendingCategoryEnum category;

    public Spending(Long id, LocalDate date, String spending, Integer amount, SpendingCategoryEnum category) {
        this.id = id;
        this.date = date;
        this.spending = spending;
        this.amount = amount;
        this.category = category;
    }

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public String getSpending() {
        return spending;
    }

    public void setSpending(String spending) {
        this.spending = spending;
    }

    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    public SpendingCategoryEnum getCategory() {
        return category;
    }

    public void setCategory(SpendingCategoryEnum category) {
        this.category = category;
    }
}
