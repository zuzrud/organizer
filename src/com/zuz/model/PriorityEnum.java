package com.zuz.model;

public enum PriorityEnum {
    BARDZO_WAZNE("Bardzo ważne"), SREDNIO_WAZNE("Średnio ważne"), MALO_WAZNE("Mało ważne");

    private String name;

    PriorityEnum(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
