package com.zuz.model;

public enum ReminderTimeEnum {
    NIE_PRZYPOMINAJ("bez przypomnienia"), MIN_PRZED_5("5 min"), MIN_PRZED_10("10 min"), MIN_PRZED_15("15 min");

    private String name;

    ReminderTimeEnum(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
