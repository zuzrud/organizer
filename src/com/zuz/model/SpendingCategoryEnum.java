package com.zuz.model;

public enum SpendingCategoryEnum {
    ACCOMODATION("zakupy spożywcze"),
    FREE_TIME("czas wolny, miasto"),
    PLATFORMS("platformy"),
    OTHER("inne");


    private String name;

    SpendingCategoryEnum(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
