package com.zuz.model;

import java.time.LocalDate;

public class ToDo implements Id {

    private Long id;
    private LocalDate date;
    private String toDo;
    private Enum<PriorityEnum> priority;
    private Boolean extra;

    public ToDo(Long id, LocalDate date, String toDo, Enum<PriorityEnum> priority, Boolean extra) {
        this.id = id;
        this.date = date;
        this.toDo = toDo;
        this.priority = priority;
        this.extra = extra;
    }

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public String getToDo() {
        return toDo;
    }

    public void setToDo(String toDo) {
        this.toDo = toDo;
    }

    public Enum<PriorityEnum> getPriority() {
        return priority;
    }

    public void setPriority(Enum<PriorityEnum> priority) {
        this.priority = priority;
    }

    public Boolean getExtra() {
        return extra;
    }

    public void setExtra(Boolean extra) {
        this.extra = extra;
    }
}
