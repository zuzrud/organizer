package com.zuz.model;

import java.time.LocalDateTime;

public class Event implements Id {

    private Long id;
    private LocalDateTime date;
    private String event;
    private Boolean reminder;
    private Enum<ReminderTimeEnum> reminderTime;

    public Event(Long id, LocalDateTime date, String event, Boolean reminder, Enum<ReminderTimeEnum> reminderTime) {
        this.id = id;
        this.date = date;
        this.event = event;
        this.reminder = reminder;
        this.reminderTime = reminderTime;
    }

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    public LocalDateTime getDate() {
        return date;
    }

    public void setDate(LocalDateTime date) {
        this.date = date;
    }

    public String getEvent() {
        return event;
    }

    public void setEvent(String event) {
        this.event = event;
    }

    public Boolean getReminder() {
        return reminder;
    }

    public Enum<ReminderTimeEnum> getReminderTime() {
        return reminderTime;
    }

}
