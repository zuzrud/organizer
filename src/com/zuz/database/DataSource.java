package com.zuz.database;

public abstract class DataSource<T> {

    public abstract T addItem(T t);

    public abstract T editItem(T t, T oldT);

    public abstract boolean deleteItem(T t);
}
