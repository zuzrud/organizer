package com.zuz.views.daywindow.spending;

import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;

import com.zuz.database.DataSource;
import com.zuz.model.Spending;

public class SpendingDataSource extends DataSource<Spending> {

    public static List<Spending> loadForDisplay(LocalDate day) {
        SpendingBase spendingBase = SpendingBase.getTable();
        return spendingBase.findAll().stream().filter(spending -> spending.getDate().equals(day)).collect(Collectors.toList());
    }

    @Override
    public Spending addItem(Spending spending) {
        return SpendingBase.getTable().saveItem(spending);
    }

    @Override
    public Spending editItem(Spending spending, Spending oldSpending) {
        spending.setDate(oldSpending.getDate());
//        spending.setExtra(oldSpending.getExtra());
        return SpendingBase.getTable().saveEditedItem(spending, oldSpending);
    }

    @Override
    public boolean deleteItem(Spending spending) {
        SpendingBase.getTable().deleteItem(spending);
        return true;
    }

}
