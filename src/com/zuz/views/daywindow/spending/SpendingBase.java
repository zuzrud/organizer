package com.zuz.views.daywindow.spending;

import java.time.LocalDate;

import org.json.simple.JSONObject;
import com.zuz.database.TableBase;
import com.zuz.model.Spending;
import com.zuz.model.SpendingCategoryEnum;

public class SpendingBase extends TableBase<Spending> {

    private static SpendingBase table;

    private SpendingBase() {
    }

    public static SpendingBase getTable() {
        if (table == null) {
            table = new SpendingBase();
            table.load();
        }
        return table;
    }

    @Override
    protected String tableName() {
        return "spending";
    }

    public Spending findById(Long id) {
        return table.getItems()
            .stream()
            .filter(spending1 -> spending1.getId().equals(id)).findAny().get();
    }

    @Override
    protected JSONObject mapToJSONObject(Spending spending) {
        JSONObject spendingDetails = new JSONObject();
        spendingDetails.put("id", spending.getId());
        spendingDetails.put("date", spending.getDate().toString());
        spendingDetails.put("spending", spending.getSpending());
        spendingDetails.put("amount", spending.getAmount().toString());
        spendingDetails.put("category", spending.getCategory().toString());

        JSONObject spendingObject = new JSONObject();
        spendingObject.put(tableName(), spendingDetails);
        return spendingObject;
    }

    @Override
    protected Spending parseItemJSONObject(JSONObject spendingJSON) {
        JSONObject spendingObject = (JSONObject) spendingJSON.get(tableName());

        Long id = (Long) spendingObject.get("id");
        LocalDate date = LocalDate.parse((String) spendingObject.get("date"));
        String spendingAction = (String) spendingObject.get("spending");
        Integer amount = Integer.valueOf((String) spendingObject.get("amount"));

        SpendingCategoryEnum category = SpendingCategoryEnum.valueOf((String) spendingObject.get("category"));

        Spending spending = new Spending(id, date, spendingAction, amount, category);
        return spending;
    }

}
