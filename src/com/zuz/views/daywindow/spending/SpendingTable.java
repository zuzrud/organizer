package com.zuz.views.daywindow.spending;

import java.util.List;
import java.util.Optional;

import com.zuz.database.DataSource;
import com.zuz.model.Spending;
import com.zuz.model.SpendingCategoryEnum;
import com.zuz.views.MainWindow;
import com.zuz.viewutil.GenericTable;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.value.ChangeListener;
import javafx.collections.transformation.FilteredList;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;

public class SpendingTable extends GenericTable<Spending> {

    private MainWindow owner;

    public SpendingTable(MainWindow owner) {
        this.owner = owner;
    }

    @Override
    protected String getTitle() {
        return "Wydatki";
    }

    @Override
    protected Optional<Spending> getEnteredData() { //add
        SpendingAdd spendingAdd = new SpendingAdd(owner.getDay());
        return spendingAdd.displayAdd();
    }

    @Override
    protected Optional<Spending> getEnteredData(Spending spending) { //edit
        SpendingAdd spendingAdd = new SpendingAdd(spending);
        return spendingAdd.displayAdd();
    }

    @Override
    public TableView<Spending> getTableView() {
        TableColumn<Spending, String> spendingColumn = new TableColumn<>("Wydatek");
        spendingColumn.setMinWidth(550);
        spendingColumn.setCellValueFactory(new PropertyValueFactory<>("spending"));

        TableColumn<Spending, String> amountColumn = new TableColumn<>("Kwota");
        amountColumn.setMinWidth(100);
        amountColumn.setCellValueFactory(new PropertyValueFactory<>("amount"));

        TableColumn<Spending, SpendingCategoryEnum> categoryColumn = new TableColumn<>("Kategoria");
        categoryColumn.setMinWidth(200);
        categoryColumn.setCellValueFactory(
            cellData -> new SimpleObjectProperty(SpendingCategoryEnum.valueOf(cellData.getValue().getCategory().name()).getName()));

        TableView<Spending> tableView = new TableView<>();

        tableView.getColumns().addAll(spendingColumn, amountColumn, categoryColumn);
        return tableView;
    }

    @Override
    public List<Spending> getDataForDisplay() {
        return SpendingDataSource.loadForDisplay(owner.getDay());
    }

    @Override
    public DataSource getDataSource() {
        return new SpendingDataSource();
    }

    @Override
    public boolean findVisible() {
        return false;
    }

    @Override
    public ChangeListener<String> getStringChangeListener(FilteredList<Spending> filteredData) {
        return null;
    }

}
