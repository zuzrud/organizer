package com.zuz.views.daywindow.spending;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.Optional;

import com.zuz.components.NumberField;
import com.zuz.model.Spending;
import com.zuz.model.SpendingCategoryEnum;
import com.zuz.viewutil.SpendingCategoryCellFactory;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.stage.Modality;
import javafx.stage.Stage;

public class SpendingAdd {

    private LocalDate day;

    private Optional<Spending> spending = Optional.empty();

    private Stage window;

    private TextField txtSpending;

    private TextField txtAmount;

    private ComboBox categoryBox;

    public SpendingAdd(LocalDate day) { //add
        this.day = day;
    }

    public SpendingAdd(Spending spending) {
        this.spending = Optional.of(spending);
    } //edit

    public void initInputs() {
        spending.ifPresent(spending1 -> {
            this.txtSpending.setText(spending1.getSpending());
            this.txtAmount.setText(spending1.getAmount().toString());
            this.categoryBox.setValue(spending1.getCategory());
        });
    }

    public Optional<Spending> displayAdd() {

        window = new Stage();
        window.initModality(Modality.APPLICATION_MODAL);

        Label lblSpending = new Label("Wydatek:");
        txtSpending = new TextField();
        txtSpending.setMinWidth(60);
        txtSpending.setPrefWidth(200);
        txtSpending.setMaxWidth(280);
        txtSpending.setPromptText("Na co zostały wydane pieniądze");

        Label lblAmount = new Label("Kwota:");
        txtAmount = new NumberField();
        txtAmount.setMinWidth(60);
        txtAmount.setPrefWidth(120);
        txtAmount.setMaxWidth(180);
        txtAmount.setPromptText("Wprowadź kwotę");

        Label lblCategory = new Label("Kategoria: ");
        categoryBox = new ComboBox<>();
        categoryBox.setMinWidth(100);
        categoryBox.setPrefWidth(300);
        categoryBox.setMaxWidth(400);
        categoryBox.getItems().addAll(Arrays.asList(SpendingCategoryEnum.values()));
        SpendingCategoryCellFactory categoryCellFactory = new SpendingCategoryCellFactory();
        categoryBox.setButtonCell(categoryCellFactory.call(null));
        categoryBox.setCellFactory(categoryCellFactory);
        categoryBox.setPromptText("Wybierz kategorię");

        // Create the buttons
        Button btnOK = new Button("OK");
        btnOK.setPrefWidth(80);
        btnOK.setOnAction(e -> btnOK_Click());

        btnOK.disableProperty().bind(
            txtSpending.textProperty().isEmpty()
                .or(categoryBox.valueProperty().isNull())
        );

        Button btnCancel = new Button("Anuluj");
        btnCancel.setPrefWidth(80);
        btnCancel.setOnAction(e -> btnCancel_Click());
        HBox paneButtons = new HBox(10, btnOK, btnCancel);

        // Create the GridPane layout
        GridPane grid = new GridPane();
        grid.setPadding(new Insets(10));
        grid.setHgap(10);
        grid.setVgap(10);
        grid.setMinWidth(500);
        grid.setPrefWidth(500);
        grid.setMaxWidth(800);
        // Add the nodes to the pane
        grid.addRow(0, lblSpending, txtSpending);
        grid.addRow(1, lblAmount, txtAmount);
        grid.addRow(2, lblCategory, categoryBox);
        grid.add(paneButtons, 2, 5);

        // Set alignments and spanning
        grid.setHalignment(lblSpending, HPos.RIGHT);
        grid.setHalignment(lblAmount, HPos.RIGHT);
        grid.setHalignment(lblCategory, HPos.RIGHT);
        grid.setColumnSpan(txtSpending, 2);
        grid.setColumnSpan(txtAmount, 2);
        grid.setColumnSpan(categoryBox, 2);

        // Set column widths
        ColumnConstraints col1 = new ColumnConstraints();
        col1.setPercentWidth(33);
        ColumnConstraints col2 = new ColumnConstraints();
        col2.setPercentWidth(33);
        ColumnConstraints col3 = new ColumnConstraints();
        col3.setPercentWidth(33);
        grid.getColumnConstraints().addAll(col1, col2, col3);

        // Create the scene and the stage
        Scene scene = new Scene(grid);
        window.setScene(scene);
        window.setTitle("Wydatek");
        window.setMinWidth(500);
        window.setMaxWidth(900);

        initInputs();

        window.showAndWait();

        return spending;
    }

    private void btnCancel_Click() {
        spending = Optional.empty();
        window.close();
    }

    private void btnOK_Click() {
        spending = Optional
            .of(new Spending(1L, day, txtSpending.getText(), Integer.valueOf(txtAmount.getText()),
                (SpendingCategoryEnum) categoryBox.getValue()));
        window.close();
    }
}


