package com.zuz.views.daywindow.event;

import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;

import com.zuz.database.DataSource;
import com.zuz.model.Event;

public class EventDataSource extends DataSource<Event> {

    public static List<Event> loadForDisplay(LocalDate day) {
        EventBase eventBase = EventBase.getTable();
        return eventBase.findAll().stream().filter(event -> event.getDate().toLocalDate().isEqual(day)).collect(Collectors.toList());
    }

    @Override
    public Event addItem(Event event) {
        return EventBase.getTable().saveItem(event);
    }

    @Override
    public Event editItem(Event event, Event oldEvent) {
        return EventBase.getTable().saveEditedItem(event, oldEvent);
    }

    @Override
    public boolean deleteItem(Event event) {
        return EventBase.getTable().deleteItem(event);
    }

}
