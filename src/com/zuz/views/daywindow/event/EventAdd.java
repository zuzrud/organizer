package com.zuz.views.daywindow.event;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.Optional;

import com.zuz.model.Event;
import com.zuz.model.ReminderTimeEnum;
import com.zuz.util.DaysHours;
import com.zuz.util.HoursMinutes;
import com.zuz.viewutil.ReminderTimeCellFactory;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.stage.Modality;
import javafx.stage.Stage;

public class EventAdd {

    private LocalDate day;

    private Optional<Event> event = Optional.empty();

    private Stage window;

    private TextField txtEvent;

    private ComboBox hourCombo;

    private ComboBox minuteCombo;

    private ComboBox reminderTimeBox;

    public EventAdd(LocalDate day) { //add
        this.day = day;
    }

    public EventAdd(Event event) {
        this.day = event.getDate().toLocalDate();
        this.event = Optional.of(event);
    } //edit

    public void initInputs() {
        event.ifPresent(event1 -> {
            this.txtEvent.setText(event1.getEvent());
            this.hourCombo.setValue(event1.getDate().getHour());
            this.minuteCombo.setValue(event1.getDate().getMinute());
            this.reminderTimeBox.setValue(event1.getReminderTime());
        });
    }

    public Optional<Event> displayAdd() {

        window = new Stage();
        window.initModality(Modality.APPLICATION_MODAL);

        Label lblEvent = new Label("Do zrobienia:");
        txtEvent = new TextField();
        txtEvent.setMinWidth(60);
        txtEvent.setPrefWidth(200);
        txtEvent.setMaxWidth(280);
        txtEvent.setPromptText("Wprowadź co masz do zrobienia");

        Label lblHour = new Label("Godzina");
        HBox hourBox = new HBox();
        hourCombo = new ComboBox<>();
        hourCombo.setPrefWidth(70);
        hourCombo.getItems().addAll(DaysHours.hours);
        Label lblHourGap = new Label(" : ");
        minuteCombo = new ComboBox<>();
        minuteCombo.setPrefWidth(70);
        minuteCombo.getItems().addAll(HoursMinutes.minutes);

        hourBox.getChildren().addAll(hourCombo, lblHourGap, minuteCombo);

        Label lblReminderTime = new Label("Kiedy przypomnieć");
        reminderTimeBox = new ComboBox<>();
        reminderTimeBox.setMinWidth(100);
        reminderTimeBox.setPrefWidth(200);
        reminderTimeBox.setMaxWidth(300);
        reminderTimeBox.getItems().addAll(Arrays.asList(ReminderTimeEnum.values()));
        ReminderTimeCellFactory reminderTimeCellFactory = new ReminderTimeCellFactory();
        reminderTimeBox.setButtonCell(reminderTimeCellFactory.call(null));
        reminderTimeBox.setCellFactory(reminderTimeCellFactory);

        // Create the buttons
        Button btnOK = new Button("OK");
        btnOK.setPrefWidth(80);
        btnOK.setOnAction(e -> btnOK_Click());

        btnOK.disableProperty().bind(
            txtEvent.textProperty().isEmpty()
                .or(hourCombo.valueProperty().isNull()
                    .or(minuteCombo.valueProperty().isNull()
                        .or(reminderTimeBox.valueProperty().isNull()))));

        Button btnCancel = new Button("Anuluj");
        btnCancel.setPrefWidth(80);
        btnCancel.setOnAction(e -> btnCancel_Click());
        HBox paneButtons = new HBox(10, btnOK, btnCancel);

        // Create the GridPane layout
        GridPane grid = new GridPane();
        grid.setPadding(new Insets(10));
        grid.setHgap(10);
        grid.setVgap(10);
        grid.setMinWidth(500);
        grid.setPrefWidth(500);
        grid.setMaxWidth(800);
        // Add the nodes to the pane
        grid.addRow(0, lblEvent, txtEvent);
        grid.addRow(1, lblHour, hourBox);
//        grid.add(paneRemainder, 1, 3);
        grid.addRow(4, lblReminderTime, reminderTimeBox);
        grid.add(paneButtons, 2, 5);

        // Set alignments and spanning
        grid.setHalignment(lblEvent, HPos.RIGHT);
        grid.setHalignment(lblHour, HPos.RIGHT);
        grid.setHalignment(lblReminderTime, HPos.RIGHT);
        grid.setColumnSpan(txtEvent, 2);
        grid.setColumnSpan(hourBox, 2);
        grid.setColumnSpan(reminderTimeBox, 2);

        // Set column widths
        ColumnConstraints col1 = new ColumnConstraints();
        col1.setPercentWidth(33);
        ColumnConstraints col2 = new ColumnConstraints();
        col2.setPercentWidth(33);
        ColumnConstraints col3 = new ColumnConstraints();
        col3.setPercentWidth(33);
        grid.getColumnConstraints().addAll(col1, col2, col3);

        // Create the scene and the stage
        Scene scene = new Scene(grid);
        window.setScene(scene);
        window.setTitle("Planowane zadanie");
        window.setMinWidth(500);
        window.setMaxWidth(900);

        initInputs();

        window.showAndWait();

        return event;
    }

    private void btnCancel_Click() {
        event = Optional.empty();
        window.close();
    }

    private void btnOK_Click() {
        LocalDateTime time = LocalDateTime.of(day.getYear(), day.getMonthValue(), day.getDayOfMonth(),
            Integer.valueOf(hourCombo.getValue().toString()), Integer.valueOf(minuteCombo.getValue().toString()));
        event = Optional
            .of(new Event(1L, time, txtEvent.getText(), true, (ReminderTimeEnum) reminderTimeBox.getValue()));
//        event = Optional
//            .of(new Event(1L, time, txtEvent.getText(),
//                rdoYes.isSelected(),
//                rdoYes.isSelected() ? remainderTimeBox.getValue() : null));
        window.close();
    }
}


