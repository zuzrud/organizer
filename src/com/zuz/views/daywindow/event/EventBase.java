package com.zuz.views.daywindow.event;

import java.time.LocalDateTime;

import org.json.simple.JSONObject;
import com.zuz.database.TableBase;
import com.zuz.model.Event;
import com.zuz.model.ReminderTimeEnum;

public class EventBase extends TableBase<Event> {

    private static EventBase table;

    private EventBase() {
    }

    public static EventBase getTable() {
        if (table == null) {
            table = new EventBase();
            table.load();
        }
        return table;
    }

    @Override
    protected String tableName() {
        return "event";
    }

    public Event findById(Long id) {
        return table.getItems()
            .stream()
            .filter(event1 -> event1.getId().equals(id)).findAny().get();
    }

    @Override
    protected JSONObject mapToJSONObject(Event event) {
        JSONObject eventDetails = new JSONObject();
        eventDetails.put("id", event.getId());
        eventDetails.put("date", event.getDate().toString());
        eventDetails.put("event", event.getEvent());
        eventDetails.put("reminder", event.getReminder().toString());
        if (event.getReminderTime() != null) {
            eventDetails.put("reminderTime", event.getReminderTime().toString());
        }

        JSONObject eventObject = new JSONObject();
        eventObject.put(tableName(), eventDetails);
        return eventObject;
    }

    @Override
    protected Event parseItemJSONObject(JSONObject eventJSON) {
        JSONObject eventObject = (JSONObject) eventJSON.get(tableName());

        Long id = (Long) eventObject.get("id");
        LocalDateTime date = LocalDateTime.parse((String) eventObject.get("date"));
        String eventAction = (String) eventObject.get("event");
        Boolean reminder = Boolean.getBoolean((String) eventObject.get("reminder"));
        ReminderTimeEnum reminderTimeEnum = ReminderTimeEnum.valueOf((String) eventObject.get("reminderTime"));

        Event event = new Event(id, date, eventAction, reminder, reminderTimeEnum);
        return event;
    }

}
