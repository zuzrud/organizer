package com.zuz.views.daywindow.event;

import java.util.List;
import java.util.Optional;

import com.zuz.database.DataSource;
import com.zuz.model.Event;
import com.zuz.model.ReminderTimeEnum;
import com.zuz.views.MainWindow;
import com.zuz.viewutil.GenericTable;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.value.ChangeListener;
import javafx.collections.transformation.FilteredList;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;

public class EventTable extends GenericTable<Event> {

    private MainWindow owner;

    public EventTable(MainWindow owner) {
        this.owner = owner;
    }

    @Override
    protected String getTitle() {
        return "Plan godzinowy";
    }

    @Override
    protected Optional<Event> getEnteredData() { //add
        EventAdd eventAdd = new EventAdd(owner.getDay());
        return eventAdd.displayAdd();
    }

    @Override
    protected Optional<Event> getEnteredData(Event event) { //edit
        EventAdd eventAdd = new EventAdd(event);
        return eventAdd.displayAdd();
    }

    @Override
    public TableView<Event> getTableView() {

        TableColumn<Event, String> hourColumn = new TableColumn<>("Godz.");
        hourColumn.setMinWidth(50);
        hourColumn
            .setCellValueFactory(
                cellData -> new SimpleStringProperty(
                    cellData.getValue().getDate().getHour() + ":" + +cellData.getValue().getDate().getMinute()));

        TableColumn<Event, String> eventColumn = new TableColumn<>("Do zrobienia");
        eventColumn.setMinWidth(700);
        eventColumn.setCellValueFactory(new PropertyValueFactory<>("event"));

        TableColumn<Event, ReminderTimeEnum> reminderTimeColumn = new TableColumn<>("Przypomnij");
        reminderTimeColumn
            .setCellValueFactory(
                cellData -> new SimpleObjectProperty(ReminderTimeEnum.valueOf(cellData.getValue().getReminderTime().name()).getName()));

        TableView<Event> tableView = new TableView<>();

        tableView.getColumns().addAll(hourColumn, eventColumn, reminderTimeColumn);
        return tableView;
    }

    @Override
    public List<Event> getDataForDisplay() {
        return EventDataSource.loadForDisplay(owner.getDay());
    }

    @Override
    public ChangeListener<String> getStringChangeListener(FilteredList<Event> filteredData) {
        return null;
    }

    @Override
    public DataSource getDataSource() {
        return new EventDataSource();
    }

    @Override
    public boolean findVisible() {
        return false;
    }

}
