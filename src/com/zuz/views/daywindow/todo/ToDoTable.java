package com.zuz.views.daywindow.todo;

import java.util.List;
import java.util.Optional;

import com.zuz.database.DataSource;
import com.zuz.model.PriorityEnum;
import com.zuz.model.ToDo;
import com.zuz.views.MainWindow;
import com.zuz.viewutil.GenericTable;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.value.ChangeListener;
import javafx.collections.transformation.FilteredList;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;

public class ToDoTable extends GenericTable<ToDo> {

    private MainWindow owner;

    public ToDoTable(MainWindow owner) {
        this.owner = owner;
    }

    @Override
    protected String getTitle() {
        return "Do zrobienia";
    }

    @Override
    protected Optional<ToDo> getEnteredData() { //add
        ToDoAdd toDoAdd = new ToDoAdd(owner.getDay());
        return toDoAdd.displayAdd();
    }

    @Override
    protected Optional<ToDo> getEnteredData(ToDo toDo) { //edit
        ToDoAdd toDoAdd = new ToDoAdd(toDo);
        return toDoAdd.displayAdd();
    }

    @Override
    public TableView<ToDo> getTableView() {
        TableColumn<ToDo, String> toDoColumn = new TableColumn<>("Do zrobienia");
        toDoColumn.setMinWidth(700);
        toDoColumn.setCellValueFactory(new PropertyValueFactory<>("toDo"));

        TableColumn<ToDo, PriorityEnum> priorityColumn = new TableColumn<>("Priorytet");
        priorityColumn.setMinWidth(100);
        priorityColumn.setCellValueFactory(
            cellData -> new SimpleObjectProperty(PriorityEnum.valueOf(cellData.getValue().getPriority().name()).getName()));


        TableColumn<ToDo, Boolean> extraColumn = new TableColumn<>("Extra");
        extraColumn.setCellValueFactory(cellData -> new SimpleBooleanProperty(cellData.getValue().getExtra()));

        extraColumn.setCellFactory(col -> new TableCell<ToDo, Boolean>() {
            @Override
            protected void updateItem(Boolean item, boolean empty) {
                super.updateItem(item, empty) ;
                setText(empty ? null : item ? "TAK" : "NIE" );
            }
        });

        TableView<ToDo> tableView = new TableView<>();

        tableView.getColumns().addAll(toDoColumn, priorityColumn, extraColumn);
        return tableView;
    }

    @Override
    public List<ToDo> getDataForDisplay() {
        return ToDoDataSource.loadForDisplay(owner.getDay());
    }

    @Override
    public ChangeListener<String> getStringChangeListener(FilteredList<ToDo> filteredData) {
        return null;
    }

    @Override
    public DataSource getDataSource() {
        return new ToDoDataSource();
    }

    @Override
    public boolean findVisible() {
        return false;
    }

}
