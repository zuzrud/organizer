package com.zuz.views.daywindow.todo;

import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;

import com.zuz.database.DataSource;
import com.zuz.model.ToDo;

public class ToDoDataSource extends DataSource<ToDo> {

    public static List<ToDo> loadForDisplay(LocalDate day) {
        ToDoBase toDoBase = ToDoBase.getTable();
        return toDoBase.findAll().stream().filter(toDo -> toDo.getDate().equals(day)).collect(Collectors.toList());
    }

    @Override
    public ToDo addItem(ToDo toDo) {
        return ToDoBase.getTable().saveItem(toDo);
    }

    @Override
    public ToDo editItem(ToDo toDo, ToDo oldToDo) {
        toDo.setDate(oldToDo.getDate());
        toDo.setExtra(oldToDo.getExtra());
        return ToDoBase.getTable().saveEditedItem(toDo, oldToDo);
    }

    @Override
    public boolean deleteItem(ToDo toDo) {
        ToDoBase.getTable().deleteItem(toDo);
        return true;
    }

}
