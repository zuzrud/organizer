package com.zuz.views.daywindow;

import com.zuz.views.MainWindow;
import com.zuz.views.daywindow.event.EventTable;
import com.zuz.views.daywindow.spending.SpendingTable;
import com.zuz.views.daywindow.todo.ToDoTable;
import com.zuz.viewutil.MakeWindowCenter;
import javafx.scene.Parent;
import javafx.scene.control.ListView;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;

public class DayWindow implements MakeWindowCenter {

    private MainWindow owner;

    private ListView<String> spendingList;

    public DayWindow(MainWindow owner) {
        this.owner = owner;
    }

    @Override
    public Pane makeCenter() {
        VBox mainLayout = new VBox();
        Parent layoutToDo = getToDo();
        Parent layoutHourly = getHourly();
        Parent layoutSpendings = getSpendings();

        mainLayout.getChildren().addAll(layoutToDo, layoutHourly, layoutSpendings);
        return mainLayout;
    }

    private Parent getToDo() {
        ToDoTable toDoTable = new ToDoTable(owner);
        return toDoTable.createView();
    }

    private Parent getHourly() {
        EventTable eventTable = new EventTable(owner);
        return eventTable.createView();
    }

    private Parent getSpendings() {
        SpendingTable spendingTable = new SpendingTable(owner);
        return spendingTable.createView();
    }

}
