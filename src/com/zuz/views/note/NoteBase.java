package com.zuz.views.note;

import com.zuz.model.Note;
import org.json.simple.JSONObject;
import com.zuz.database.TableBase;

public class NoteBase extends TableBase<Note> {

    private static final String FILE_NAME = "note";
    private static final String ID_FIELD = "id";
    private static final String TITLE_FIELD = "title";
    private static final String CONTENT_FIELD = "content";

    private static NoteBase table;

    private NoteBase() {
    }

    public static NoteBase getTable() {
        if (table == null) {
            table = new NoteBase();
            table.load();
        }
        return table;
    }

    @Override
    protected String tableName() {
        return FILE_NAME;
    }

    public Note findById(Long id) {
        return table.getItems()
                .stream()
                .filter(note1 -> note1.getId().equals(id)).findAny().get();
    }

    @Override
    protected JSONObject mapToJSONObject(Note note) {
        JSONObject noteDetails = new JSONObject();
        noteDetails.put(ID_FIELD, note.getId());
        noteDetails.put(TITLE_FIELD, note.getTitle());
        noteDetails.put(CONTENT_FIELD, note.getContent());

        JSONObject noteObject = new JSONObject();
        noteObject.put(tableName(), noteDetails);
        return noteObject;
    }

    @Override
    protected Note parseItemJSONObject(JSONObject noteJSON) {
        JSONObject noteObject = (JSONObject) noteJSON.get(tableName());

        Long id = (Long) noteObject.get(ID_FIELD);
        String title = (String) noteObject.get(TITLE_FIELD);
        String content = (String) noteObject.get(CONTENT_FIELD);

        Note note = new Note(id, title, content);
        return note;
    }

}
