package com.zuz.views.note;

import java.util.List;

import com.zuz.database.DataSource;
import com.zuz.model.Note;

public class NoteDataSource extends DataSource<Note> {

    public static List<Note> loadForDisplay() {
        NoteBase noteBase = NoteBase.getTable();
        return noteBase.findAll();
    }

    @Override
    public Note addItem(Note note) {
        return NoteBase.getTable().saveItem(note);
    }

    @Override
    public Note editItem(Note note, Note oldNote) {
        return NoteBase.getTable().saveEditedItem(note, oldNote);
    }

    @Override
    public boolean deleteItem(Note note) {
        return NoteBase.getTable().deleteItem(note);
    }

}
