package com.zuz.views;

import java.time.LocalDate;

import com.zuz.navigator.Navigator;
import com.zuz.views.daywindow.DayWindow;
import com.zuz.views.monthwindow.MonthWindow;
import com.zuz.views.note.NoteTable;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Pane;

public class MainWindow {

    private LocalDate day;

    private BorderPane borderPane;
    private YearWindow yearWindow;
    private MonthWindow monthWindow;
    private DayWindow dayWindow;
    private Navigator navigator;
    private NoteTable noteTable;

    public Scene makeScene() {
        navigator = new Navigator(this);
        yearWindow = new YearWindow(this);
        monthWindow = new MonthWindow(this);
        dayWindow = new DayWindow(this);
        noteTable = new NoteTable();

        borderPane = new BorderPane();
        borderPane.setTop(navigator.getMainMenu());
        setDay(LocalDate.now());

        borderPane.setCenter(yearWindow.makeCenter());

        return new Scene(borderPane, 900, 900);
    }

    public void setDay(LocalDate day){
        this.day = day;
        navigator.setDay(day);
    }

    public DayWindow getDayWindow() {
        return dayWindow;
    }

    public YearWindow getYearWindow() {
        return yearWindow;
    }

    public MonthWindow getMonthWindow() {
        return monthWindow;
    }

    public void setCenter(Pane view) {
        borderPane.setCenter(view);
    }

    public Integer getYear() {
        return day.getYear();
    }

    public Integer getMonth() {
        return day.getMonthValue();
    }

    public LocalDate getDay() {
        return day;
    }

    public NoteTable getNoteTable() {
        return noteTable;
    }
}
