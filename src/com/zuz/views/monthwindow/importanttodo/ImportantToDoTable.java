package com.zuz.views.monthwindow.importanttodo;

import java.util.List;
import java.util.Optional;

import com.zuz.database.DataSource;
import com.zuz.model.ToDo;
import com.zuz.views.MainWindow;
import com.zuz.viewutil.GenericTable;
import javafx.beans.value.ChangeListener;
import javafx.collections.transformation.FilteredList;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;

public class ImportantToDoTable extends GenericTable<ToDo> {

    private MainWindow owner;

    public ImportantToDoTable(MainWindow owner) {
        this.owner = owner;
    }

    @Override
    protected String getTitle() {
        return "Ważne";
    }

    @Override
    protected Optional<ToDo> getEnteredData() { //add
        ImportantToDoAdd toDoAdd = new ImportantToDoAdd();
        return toDoAdd.displayAdd();
    }

    @Override
    protected Optional<ToDo> getEnteredData(ToDo toDo) { //edit
        ImportantToDoAdd toDoAdd = new ImportantToDoAdd(toDo);
        return toDoAdd.displayAdd();
    }

    @Override
    public TableView<ToDo> getTableView() {
        TableColumn<ToDo, String> dateColumn = new TableColumn<>("Dzień");
        dateColumn.setMinWidth(100);
        dateColumn.setCellValueFactory(new PropertyValueFactory<>("date"));

        TableColumn<ToDo, String> toDoColumn = new TableColumn<>("Do zrobienia");
        toDoColumn.setMinWidth(400);
        toDoColumn.setCellValueFactory(new PropertyValueFactory<>("toDo"));

        TableView<ToDo> tableView = new TableView<>();
        tableView.getColumns().addAll(dateColumn, toDoColumn);
        return tableView;
    }

    @Override
    public List<ToDo> getDataForDisplay() {
        return ImportantToDoDataSource.loadForDisplay(owner.getDay());
    }

    @Override
    public ChangeListener<String> getStringChangeListener(FilteredList<ToDo> filteredData) {
        return null;
    }

    @Override
    public DataSource getDataSource() {
        return new ImportantToDoDataSource();
    }

    @Override
    public boolean findVisible() {
        return false;
    }


}
