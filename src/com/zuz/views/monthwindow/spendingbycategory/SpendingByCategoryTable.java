package com.zuz.views.monthwindow.spendingbycategory;

import java.util.List;
import java.util.Optional;

import com.zuz.database.DataSource;
import com.zuz.model.SpendingCategoryEnum;
import com.zuz.views.MainWindow;
import com.zuz.views.daywindow.spending.SpendingBase;
import com.zuz.viewutil.GenericTable;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.value.ChangeListener;
import javafx.collections.transformation.FilteredList;
import javafx.scene.Parent;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;

public class SpendingByCategoryTable extends GenericTable<SpendingByCategoryDTO> {

    private MainWindow owner;

    public SpendingByCategoryTable(MainWindow owner) {
        this.owner = owner;
    }

    @Override
    protected String getTitle() {
        SpendingBase spendingBase = SpendingBase.getTable();
        Integer sum =
            spendingBase
                .findAll()
                .stream()
                .filter(spending -> spending.getDate().getYear() == owner.getDay().getYear()
                        && spending.getDate().getMonthValue() == owner.getDay().getMonthValue())
                .reduce(0, (sub, spending2) -> sub + spending2.getAmount(), Integer::sum);
        return "Razem: " + sum + " zł";
    }

    @Override
    protected Optional<SpendingByCategoryDTO> getEnteredData() { //add
        return null;
    }

    @Override
    protected Optional<SpendingByCategoryDTO> getEnteredData(SpendingByCategoryDTO spending) { //edit
        return null;
    }

    @Override
    public TableView<SpendingByCategoryDTO> getTableView() {
        TableColumn<SpendingByCategoryDTO, SpendingCategoryEnum> categoryColumn = new TableColumn<>("Kategoria");
        categoryColumn.setMinWidth(200);
        categoryColumn.setCellValueFactory(
            cellData -> new SimpleObjectProperty(SpendingCategoryEnum.valueOf(cellData.getValue().getCategory().name()).getName()));

        TableColumn<SpendingByCategoryDTO, String> amountColumn = new TableColumn<>("Kwota");
        amountColumn.setMinWidth(100);
        amountColumn.setCellValueFactory(new PropertyValueFactory<>("amount"));


        TableView<SpendingByCategoryDTO> tableView = new TableView<>();

        tableView.getColumns().addAll(categoryColumn, amountColumn);
        return tableView;
    }

    @Override
    public List<SpendingByCategoryDTO> getDataForDisplay() {
        return SpendingByCategoryDataSource.loadForDisplay(owner.getDay());
    }

    @Override
    public DataSource getDataSource() {
        return new SpendingByCategoryDataSource();
    }

    @Override
    public boolean findVisible() {
        return false;
    }

    @Override
    public ChangeListener<String> getStringChangeListener(FilteredList<SpendingByCategoryDTO> filteredData) {
        return null;
    }

    @Override
    protected Parent getBottom() {
        return null;
    }

}
