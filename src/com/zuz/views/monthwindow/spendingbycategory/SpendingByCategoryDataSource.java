package com.zuz.views.monthwindow.spendingbycategory;

import static java.util.stream.Collectors.*;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.zuz.database.DataSource;
import com.zuz.model.Spending;
import com.zuz.model.SpendingCategoryEnum;
import com.zuz.views.daywindow.spending.SpendingBase;

public class SpendingByCategoryDataSource extends DataSource<SpendingByCategoryDTO> {

    public static List<SpendingByCategoryDTO> loadForDisplay(LocalDate day) {
        SpendingBase spendingBase = SpendingBase.getTable();
        //Grupowanie wg kategorii
        //https://www.baeldung.com/java-groupingby-collector
        Map<SpendingCategoryEnum, Integer> categoryEnumIntegerMap = spendingBase
            .findAll()
            .stream()
            .filter(spending -> spending.getDate().getYear() == day.getYear()
                && spending.getDate().getMonthValue() == day.getMonthValue())
            .collect(groupingBy(Spending::getCategory, summingInt(Spending::getAmount)));

        List<SpendingByCategoryDTO> categoryDTOs = new ArrayList<>();
        //Sumowanie wg kategorii
        //https://www.baeldung.com/java-stream-reduce
        categoryEnumIntegerMap
            .forEach((spendingCategoryEnum, amount) -> categoryDTOs.add(new SpendingByCategoryDTO(spendingCategoryEnum, amount)));
        return categoryDTOs;
    }

    @Override
    public SpendingByCategoryDTO addItem(SpendingByCategoryDTO spendingByCategoryDTO) {
        return null;
    }

    @Override
    public SpendingByCategoryDTO editItem(SpendingByCategoryDTO spendingByCategoryDTO, SpendingByCategoryDTO oldSpendingByCategoryDTO) {
        return null;
    }

    @Override
    public boolean deleteItem(SpendingByCategoryDTO spendingByCategoryDTO) {
        return true;
    }

}
