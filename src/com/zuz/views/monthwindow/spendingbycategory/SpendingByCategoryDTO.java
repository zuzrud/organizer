package com.zuz.views.monthwindow.spendingbycategory;

import com.zuz.model.SpendingCategoryEnum;

public class SpendingByCategoryDTO {

    private SpendingCategoryEnum category;

    private Integer amount;

    public SpendingByCategoryDTO(SpendingCategoryEnum category, Integer amount) {
        this.category = category;
        this.amount = amount;
    }

    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    public SpendingCategoryEnum getCategory() {
        return category;
    }

    public void setCategory(SpendingCategoryEnum category) {
        this.category = category;
    }
}
