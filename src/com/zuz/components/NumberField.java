package com.zuz.components;

import java.util.regex.Pattern;

import javafx.scene.control.TextField;

public class NumberField extends TextField {

    private final static Pattern pattern = Pattern.compile("^\\d$");

    // wzorzec ^\\d$ oznacza że to mogą być tylko liczby


    @Override
    public void replaceText(int start, int end, String text) {
        if (pattern.matcher(text).matches() || text.isEmpty()) {
            super.replaceText(start, end, text);
        }
    }

    @Override
    public void replaceSelection(String replacement) {
        super.replaceSelection(replacement);
    }
}
