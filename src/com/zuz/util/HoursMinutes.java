package com.zuz.util;

import java.util.ArrayList;

public class HoursMinutes {

    public static ArrayList<String> minutes;

    static {
        minutes = new ArrayList<>();
        for (int i = 0; i<=59; i++)  {
            minutes.add(Integer.valueOf(i).toString());
        }
    }
}
