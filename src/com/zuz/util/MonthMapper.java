package com.zuz.util;

import java.util.HashMap;

public final class MonthMapper {

    public static HashMap<Integer, String> months;
    public static HashMap<String, Integer> monthNumbers;

    static {
        months = new HashMap<>();
        months.put(1, "Styczeń");
        months.put(2, "Luty");
        months.put(3, "Marzec");
        months.put(4, "Kwiecień");
        months.put(5, "Maj");
        months.put(6, "Czerwiec");
        months.put(7, "Lipiec");
        months.put(8, "Sierpień");
        months.put(9, "Wrzesien");
        months.put(10, "Październik");
        months.put(11, "Listopad");
        months.put(12, "Grudzień");

        monthNumbers = new HashMap<>();
        months.forEach((monthNumber, monthName) -> monthNumbers.put(monthName, monthNumber));
    }

}
