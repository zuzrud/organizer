package com.zuz.util;

import java.util.ArrayList;

public class DaysHours {

    public static ArrayList<String> hours;

    static {
        hours = new ArrayList<>();
        for (int i = 0; i<=23; i++)  {
            hours.add(Integer.valueOf(i).toString());
        }
    }
}
